self.addEventListener('activate', function (event) {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('push', function (event) {
  const promiseAction = self.clients.matchAll().then(function (clientList) {
    const data = event.data.json()
    const notification = {
      body: data.body,
      icon: '/logo.png',
      badge: '/logo.png',
      data: {
        dateOfArrival: Date.now(),
      }
    }

    if (clientList.length === 0) {
      // Show notification, App ain't opened      
      self.registration.showNotification(data.title, notification)
    } else {
      var visibleTab = clientList.find(function (c) {
        return c.visibilityState === 'visible';
      })

      if (visibleTab) {
        // App is already open and visible!, Send a message to the page to update the UI
        for (let i = 0; i < clientList.length; i++) {
          const client = clientList[i]
          client.postMessage({
            ...data,
            url: client.url
          })
        }
      } else {
        // Show notification, app is already open but invisible
        self.registration.showNotification(data.title, notification)
      }
    }
  })

  event.waitUntil(promiseAction)
})

self.addEventListener('notificationclick', function (event) {
  const clickedNotification = event.notification;
  clickedNotification.close();

  // Do something as the result of the notification click
  const promiseAction = self.clients.matchAll().then(function (clientList) {
    if (clientList.length > 0) {
      return clientList[0].focus()
    }
    return self.clients.openWindow('https://lagarosa.co/orders')
  });

  event.waitUntil(promiseAction);
});
