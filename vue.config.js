module.exports = {
  chainWebpack: config => {
    config.module.rules.delete('eslint')
    config.plugins.delete('prefetch')

    config.module.rule('pdf')
      .test(/\.(pdf)(\?.*)?$/)
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/pdf/[name].[hash:8].[ext]',
      })

    config.plugin('preload').tap((options) => {
      options[0].include = 'allChunks'
      return options
    })
  },

  devServer: {
    disableHostCheck: true,
  },

  transpileDependencies: ['vuetify'],

  pluginOptions: {
    i18n: {
      locale: 'es',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },

  configureWebpack: {
    devtool: 'source-map',
  },

}
