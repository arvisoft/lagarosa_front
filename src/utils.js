const formatMoney = (value, minDecimals = 0) => {
  const formatter = new Intl.NumberFormat('en-En', {
    style: 'currency',
    currency: 'USD',
    maximumFractionDigits: 2,
    minimumFractionDigits: minDecimals,
  })

  return formatter.format(value || 0)
}

const getPaymentMethod = (order) => {
  switch (order.payment_method) {
    case 1:
      return formatMoney(order.annotations)
    case 2:
      return 'Dátafono'
    case 3:
      return 'Crédito'
    case 4:
      return 'PSE'
    case 5:
      return 'Nequi'
    case 6:
      return 'Transferencia'
    case 7:
      return 'QR'
  }
}

const kFormatter = (num) => {
  return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num) / 1000).toFixed(1)) + 'k' : Math.sign(num) * Math.abs(num)
}

const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

const unregisterServiceWorker = () => {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistrations().then(function (registrations) {
      for (let registration of registrations) {
        registration.unregister()
          .then(function () {
            return self.clients.matchAll();
          })
          .then(function (clients) {
            clients.forEach(client => {
              if (client.url && "navigate" in client) {
                client.navigate(client.url);
              }
            });
          })
      }
    });
  }
}

const getParams = (str) => {
  var queryString = str || window.location.search || '';
  var keyValPairs = [];
  var params = {};
  queryString = queryString.replace(/.*?\?/, "");

  if (queryString.length) {
    var keyValPairs = queryString.split('&');
    for (var pairNum in keyValPairs) {
      var key = keyValPairs[pairNum].split('=')[0];
      if (!key.length) continue;
      if (typeof params[key] === 'undefined') {
        params[key] = [];
      }
      params[key].push(keyValPairs[pairNum].split('=')[1]);
    }
  }
  return params;
}

export {
  getParams,
  kFormatter,
  formatMoney,
  getPaymentMethod,
  capitalizeFirstLetter,
  unregisterServiceWorker,
}
