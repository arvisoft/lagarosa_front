import Vue from 'vue'
import VueI18n from 'vue-i18n'

import es from 'vuetify/lib/locale/es'
import en from 'vuetify/lib/locale/en'

Vue.use(VueI18n)

const messages = {
  en: {
    ...require('@/locales/en.json'),
    $vuetify: en,
  },
  es: {
    ...require('@/locales/es.json'),
    $vuetify: es,
  },
}

// Validación para saber que idioma mostrar
const currentLang = localStorage.getItem('usr-lang')
const browserLang = navigator.language.substr(0, 2).toLowerCase()
if (!currentLang) localStorage.setItem('usr-lang', browserLang !== 'en' ? 'es' : 'en')

export default new VueI18n({
  locale: 'es', // currentLang,
  fallbackLocale: 'es', // currentLang,
  messages,
})
