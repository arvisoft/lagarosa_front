import UrlCore from './UrlCore'
const baseApiUrl = process.env.VUE_APP_BASE_API_URL

export const AuthUrls = {
  login: `${UrlCore.auth}login`,
  logout: `${UrlCore.auth}logout/`,
  register: `${UrlCore.auth}register/`,
  changeProfile: `${UrlCore.auth}profile/`,
  resetPassword: `${UrlCore.auth}reset-password/`,
  changePassword: `${UrlCore.auth}change-password/`,
  socialSignInAndLogin: `${UrlCore.auth}social_signup`,
  verifyRegistration: `${UrlCore.auth}verify-registration/`,
  sendResetPasswordLink: `${UrlCore.auth}send-reset-password-link/`,
}

export const GralInfoUrls = {
  city: `${UrlCore.store}city`,
  faqs: `${UrlCore.generalInfo}faqs`,
  team: `${UrlCore.generalInfo}team`,
  config: `${UrlCore.generalInfo}config`,
  salesPoint: `${UrlCore.store}sales-point`,
  savePushSubscription: `${UrlCore.webPush}`,
  sendComplaint: `${UrlCore.generalInfo}contact`,
}

export const StoreUrls = {
  order: `${UrlCore.store}order`,
  wompiBankList: `${UrlCore.wompi}banks`,
  approveOrder: `${UrlCore.wompi}approve_order`,
  productFlavor: `${UrlCore.store}product-flavor`,
  productBySalePoint: `${UrlCore.store}inventory`,
  productCategory: `${UrlCore.store}product-category`,
  wompiTokenAcceptance: `${UrlCore.wompi}token_acceptance`,
}

export const UserUrls = {
  address: `${UrlCore.user}address`,
  subscriptors: `${UrlCore.user}subscriptors`,
  distanceForDelivery: `${UrlCore.user}address/calculate_km`,
}

export const AdminUrls = {
  users: `${baseApiUrl}users`,
  roles: `${UrlCore.user}roles`,
  audit: `${UrlCore.store}audit`,
  seating: `${UrlCore.store}seating`,
  product: `${UrlCore.store}product`,
  statistic: `${UrlCore.store}statistic`,
  orderDetail: `${UrlCore.store}orderdetail`,
  notification: `${baseApiUrl}notifications`,
}
