import Vue from 'vue'
import cryptoJs from "crypto-js";
import * as urlConstants from '@/UrlConstants'
import {
  getParams,
  kFormatter,
  formatMoney,
  unregisterServiceWorker,
} from '@/utils'
import {
  CLEAR_CART,
}
from '@/store/cart-constants'


Vue.mixin({
  mounted() {
    if (this.$vuetify.breakpoint) {
      this.isDesktop = this.$vuetify.breakpoint.mdAndUp;
    } else {
      this.isDesktop = screen.width > 960
    }
  },

  data: () => ({
    urlConstants,
    isDesktop: false,
  }),
  watch: {
    "$vuetify.breakpoint.mdAndUp"(val) {
      this.isDesktop = val;
    },
  },
  filters: {
    money(value, minDecimals = 0) {
      return formatMoney(value, minDecimals)
    },
    date(value, inFormat, outFormat) {
      return Vue.moment(value, inFormat || 'YYYY-MM-DD', true).isValid() ?
        Vue.moment(value, inFormat || 'YYYY-MM-DD').format(outFormat || 'ddd DD MMM YYYY') :
        value || '--'
    },
    datetime(value, inFormat, outFormat) {
      return Vue.moment(value, inFormat || 'YYYY-MM-DD HH:mm:ss', true).isValid() ?
        Vue.moment(value, inFormat || 'YYYY-MM-DD HH:mm:ss').format(outFormat || 'ddd DD MMM YYYY LT') :
        value || '--'
    },
    booleanToString(value) {
      return !value ? 'No' : 'Si'
    },
    kFormatter(val) {
      return kFormatter(val)
    },
  },
  methods: {
    encrypt(text) {
      return cryptoJs.DES.encrypt(text, process.env.VUE_APP_BASE_ENCRYPT_KEY).toString()
    },
    decrypt(cipertext) {
      const bytes = cryptoJs.DES.decrypt(cipertext.toString(), process.env.VUE_APP_BASE_ENCRYPT_KEY)
      return bytes.toString(cryptoJs.enc.Utf8)
    },
    createSession(data) {
      this.$session.start()
      this.$session.set('session-info', this.encrypt(JSON.stringify({
        ...data,
        showDialogInventory: true
      })))

      return this.$router.push({
        name: (data.is_superuser || data.is_staff) ? 'Dashboard' : 'HomeIndex',
      })
    },
    getSessionInfo(field) {
      const session = localStorage.getItem(process.env.VUE_APP_SESSION_KEY)
      if (session) {
        const sessionInfo = JSON.parse(
          this.decrypt(this.$session.get('session-info')),
        )

        return field ? sessionInfo[field] : sessionInfo
      }

      return undefined
    },
    appendSessionInfo(key, value) {
      const sessionInfo = this.getSessionInfo()
      sessionInfo[key] = value

      this.$session.set(
        'session-info',
        this.encrypt(JSON.stringify(sessionInfo)),
      )
    },
    destroySession() {
      this.axios.post(urlConstants.AuthUrls.logout, {
        revoke_token: true,
      }).then().finally(() => {
        this.$session.destroy()
        this.$store.dispatch(CLEAR_CART)
        localStorage.clear()
        location.reload()

        unregisterServiceWorker()
      })
    },
    round(value) {
      if (Number.isNaN(Number(value))) return 0

      return Number((Number(value * 100) / 100).toFixed(2))
    },
    getDateInputLocale() {
      const lang = 'es' // localStorage.getItem('usr-lang');
      return `${lang}-${lang.toUpperCase()}`
    },
    formatMoney(value, minDecimals) {
      return formatMoney(value, minDecimals)
    },
    formatDate(val) {
      return val ? Vue.moment(val).format('ddd DD MMM YYYY') : val
    },
    prepareFacebookApi() {
      const [fbAappId, fbApiVersion] = [process.env.VUE_APP_FACEBOOK_APP_ID, process.env.VUE_APP_FACEBOOK_API_VERSION]
      window.fbAsyncInit = function () {
        FB.init({
          appId: `${fbAappId}`,
          cookie: true,
          xfbml: true,
          version: `${fbApiVersion}`,
        })

        FB.AppEvents.logPageView()
      };

      (function (d, s, id) {
        var js;
        var fjs = d.getElementsByTagName(s)[0]
        if (d.getElementById(id)) {
          return
        }
        js = d.createElement(s)
        js.id = id
        js.src = 'https://connect.facebook.net/en_US/sdk.js'
        fjs.parentNode.insertBefore(js, fjs)
      }(document, 'script', 'facebook-jssdk'))
    },
    prepareGoogleApi() {
      window.gapi.load('auth2', () => {
        window.auth2 = window.gapi.auth2.init({
          client_id: process.env.VUE_APP_GOOGLE_APP_ID,
        })
      })
    },
    async showRecaptcha(show = true) {
      await this.$recaptchaLoaded()
      if (show) this.$recaptchaInstance.showBadge()
      else this.$recaptchaInstance.hideBadge()
    },
    getSignInErrors(error) {
      var errorMsg = ''
      if (error.response.data) {
        const data = error.response.data
        for (const key in data) {
          switch (key) {
            case 'email':
            case 'username':
              errorMsg = errorMsg.concat(
                'La dirección de correo no es válida o ya se encuentra registrada',
                ' ',
              )
              break
            case 'password':
              errorMsg = errorMsg.concat(
                'La contraseña no cumple con los requisitos, ',
                'recuerda que no debe parecerse al correo y debe incluir al menos una letra y un número',
                ' ',
              )
              break
            case 'recaptcha_token':
              errorMsg = errorMsg.concat('Captcha inválido', ' ')
              break
            case 'phone':
              errorMsg = errorMsg.concat('Ya existe un usuario registrado con el número telefónico ingresado.', ' ')
              break
          }
        }
      } else errorMsg = error

      return errorMsg || 'Credenciales inválidas'
    },
    serialize(obj) {
      var string = []

      for (var p in obj) {
        if (obj.hasOwnProperty(p)) string.push(`${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`)
      }

      return string.join('&')
    },
    getQueryParams(url, key) {
      const params = getParams(url)
      return key && params[key] || params
    }
  },
})
