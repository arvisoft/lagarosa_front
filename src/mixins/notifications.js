import {
  omit
} from "lodash";

import {
  UNREAD_NOTIFICATION
} from "@/store/dashboard";

import {
  mapState
} from "vuex";

export default {
  data() {
    return {
      soundOnRepeat: null,
      notification: {
        show: false,
        content: "test",
        type: "info",
      },
      serviceWorkerRegistation: null,
      requestNotificationPermission: false,
    }
  },
  computed: mapState([UNREAD_NOTIFICATION]),
  created() {
    this.unwatch = this.$store.watch(
      (state, getters) => getters[UNREAD_NOTIFICATION],
      (newValue, oldValue) => {
        if (!oldValue && newValue) {
          this.soundOnRepeat = setInterval(this.playNotification, 2500);
        } else if (!newValue && this.soundOnRepeat) {
          clearInterval(this.soundOnRepeat)
        }
      },
    );

    this.listenForMessages()
  },
  beforeDestroy() {
    this.unwatch();
  },
  methods: {
    notify(type = "info", content = "", show = true) {
      Object.assign(this.notification, {
        type,
        show,
        content,
      })
    },
    closeNotification() {
      this.notify('', '', false)
    },
    playNotification() {
      var audio = new Audio(require('@/assets/notification.mp3'))
      audio.volume = 1
      audio.play()
    },
    listenForMessages() {
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.addEventListener('message', (event) => {
          if (event.origin === process.env.VUE_APP_BASE_URL) {
            this.notify('info', event.data.body)
            // this.playNotification()
            this.$store.commit(UNREAD_NOTIFICATION, true)
          }
        })
      }
    },
    urlBase64ToUint8Array(base64String) {
      const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
      const base64 = (base64String + padding)
        .replace(/\-/g, "+")
        .replace(/_/g, "/");
      const rawData = window.atob(base64);
      const outputArray = new Uint8Array(rawData.length);
      const outputData = outputArray.map((output, index) =>
        rawData.charCodeAt(index)
      );

      return outputData;
    },
    checkPushApi() {
      if (
        "serviceWorker" in navigator &&
        "PushManager" in window &&
        Notification.permission !== "granted"
      ) {
        this.requestNotificationPermission = true;
      } else {
        this.registerServiceWorker();
      }
    },
    async registerServiceWorker() {
      this.requestNotificationPermission = false;
      this.serviceWorkerRegistation = await navigator.serviceWorker.register(
        `${process.env.BASE_URL}service-worker.js`
      );

      if (!this.serviceWorkerRegistation.showNotification) {
        this.notify("info", "Tu navegador no soporta las notificaciones");
        return;
      }

      this.listenForMessages();
      this.subscribeUser(this.serviceWorkerRegistation);
    },
    async subscribeUser(registration) {
      const subscription = await registration.pushManager.getSubscription();

      if (subscription) {
        this.saveSubscription(subscription, false);
        return;
      }
      // create new subscription for this browser on this device
      const vapidPublicKey = process.env.VUE_APP_VAPID_PUBLIC_KEY;
      const convertedVapidPublicKey = this.urlBase64ToUint8Array(
        vapidPublicKey
      );

      const options = {
        userVisibleOnly: true,
        applicationServerKey: convertedVapidPublicKey,
      };

      const newSubscription = await registration.pushManager.subscribe(options);
      this.saveSubscription(newSubscription);
    },
    saveSubscription(subscription, showInitial = false) {
      const browser = navigator.userAgent
        .match(/(firefox|msie|chrome|safari|trident)/gi)[0]
        .toLowerCase();

      const axiosCfg = {
        method: "post",
        url: this.urlConstants.GralInfoUrls.savePushSubscription,
        data: {
          browser,
          status_type: "subscribe",
          subscription: {
            ...omit(subscription.toJSON(), ["expirationTime"]),
          },
          group: this.getSessionInfo("sale_point"),
        },
      };

      this.axios(axiosCfg)
        .then((response) => {
          if (showInitial) {
            this.showInitialNotification();
          }
        })
        .catch((error) =>
          this.notify(
            "warning",
            "No pudimos registrar la información para enviarte notificaciones"
          )
        );
    },
    showInitialNotification() {
      this.serviceWorkerRegistation.showNotification(
        "¡Notificaciones activadas!", {
          icon: "/logo.png",
          badge: "/logo.png",
          vibrate: [300, 200, 300],
          body: "Cuando se genere un pedido recibirás una notificación como esta",
        }
      );
    },
  },
}
