const baseApiUrl = process.env.VUE_APP_BASE_API_URL

export default {
  user: `${baseApiUrl}user/`,
  auth: `${baseApiUrl}auth/`,
  wompi: `${baseApiUrl}wompi/`,
  store: `${baseApiUrl}store/`,
  webPush: `${baseApiUrl}webpush/`,
  generalInfo: `${baseApiUrl}info/`,
}
