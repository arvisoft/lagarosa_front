import Vue from 'vue'
import Router from 'vue-router'
import store from './store/index'
import cryptoJs from "crypto-js";
import {
  LOAD_CART_FROM_STORAGE,
  IN_PROGRESS_ORDER,
} from './store/cart-constants'
import {
  CUSTOMER,
} from './store/dashboard'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0,
      offset: {
        x: 0,
        y: 25,
      },
    }
  },
  routes: [{
      path: '/',
      name: 'home',
      component: () => import('@/views/pages/Index'),
      redirect: {
        name: 'HomeIndex',
      },
      children: [{
          path: '/',
          name: 'HomeIndex',
          component: () => import('@/views/pages/store/Catalogue'),
        },
        {
          path: 'puntos-de-venta',
          name: 'SalesPoints',
          component: () => import('@/views/pages/SalesPoints'),
        },
        {
          path: 'preguntas-frequentes',
          name: 'Faqs',
          component: () => import('@/views/pages/Faqs'),
        },

        {
          path: 'horarios',
          name: 'Schedules',
          component: () => import('@/views/pages/Schedules'),
        },
        {
          path: 'contactanos/quejas-y-reclamos',
          name: 'Complaints&Claims',
          component: () => import('@/views/pages/ComplaintsAndClamins'),
        },
        {
          props: true,
          name: 'MakeYourBread',
          path: 'arma-tu-pan/:inventoryId',
          component: () => import('@/views/pages/store/MakeYourBread'),
        },
        {
          props: true,
          name: 'ProductDetail',
          path: 'pedidos/:product/:inventoryId',
          component: () => import('@/views/pages/store/ProductDetail'),
        },
        {
          path: 'account',
          name: 'Account',
          meta: {
            requiresAuth: true,
          },
          component: () => import('@/views/pages/account/Main'),
        },
        {
          path: 'checkout',
          name: 'Checkout',
          component: () => import('@/views/pages/checkout/Index'),
        },
        {
          path: 'order-confirmation',
          name: 'OrderConfirmation',
          component: () => import('@/views/pages/checkout/Confirmation'),
        },
        {
          props: true,
          name: 'OrderTracing',
          path: 'order-tracing/:orderId',
          component: () => import('@/views/pages/OrderTracing'),
        },
        {
          props: true,
          path: 'rate-order/:orderId',
          name: 'RateOrder',
          meta: {
            requiresAuth: true,
          },
          component: () => import('@/views/pages/RateOrder'),
        },
        {
          path: 'order/status',
          name: 'OrderPseStatus',
          component: () => import('@/views/pages/checkout/Confirmation'),
        },
        {
          path: 'mantenimiento',
          name: 'Maintenance',
          component: () => import('@/views/pages/Maintenance'),
        },
      ],
    },
    {
      path: '/',
      meta: {
        admin: true,
        requiresAuth: true,
      },
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          path: 'dashboard',
          name: 'Dashboard',
          meta: {
            title: 'Estadisticas',
          },
          component: () => import('@/views/dashboard/statistics/Index'),
        },
        // Orders
        {
          path: 'orders',
          name: 'Orders',
          meta: {
            title: 'Pedidos',
          },
          component: () => import('@/views/dashboard/orders/Index'),
        },
        {
          name: 'OrderDetail',
          path: 'orders/:orderId',
          meta: {
            title: 'Pedido ',
          },
          component: () => import('@/views/dashboard/orders/Detail'),
        },
        {
          name: 'CreateOrder',
          path: 'orders/create/:deliveryType',
          meta: {
            title: 'Nuevo Pedido ',
          },
          component: () => import('@/views/dashboard/orders/Create'),
        },
        // Inventory
        {
          path: 'inventory',
          name: 'Inventory',
          meta: {
            title: 'Inventario',
          },
          component: () => import('@/views/dashboard/inventory/Index'),
        },
        {
          name: 'EditInventory',
          path: 'inventory/edit/:salePointId',
          meta: {
            title: 'Editar inventario',
          },
          component: () => import('@/views/dashboard/inventory/Edit'),
        },
        // Products
        {
          name: 'Products',
          path: 'products',
          meta: {
            title: 'Productos',
          },
          component: () => import('@/views/dashboard/products/Index'),
        },
        {
          name: 'CreateProduct',
          path: 'products/create',
          meta: {
            title: 'Crear Producto',
          },
          component: () => import('@/views/dashboard/products/CreateEdit'),
        },
        {
          name: 'EditProduct',
          path: 'products/edit/:productId',
          meta: {
            title: 'Editar Producto',
          },
          component: () => import('@/views/dashboard/products/CreateEdit'),
        },
        // Users
        {
          name: 'Users',
          path: 'users',
          meta: {
            title: 'Usuarios',
          },
          component: () => import('@/views/dashboard/users/Index'),
        },
        {
          name: 'Customers',
          path: 'users/customers',
          meta: {
            title: 'clientes',
          },
          component: () => import('@/views/dashboard/users/Customers'),
        },
        {
          name: 'CustomerDetail',
          path: 'users/customers/:userId',
          meta: {
            title: '',
          },
          component: () => import('@/views/dashboard/users/CustomerDetail'),
        },
        {
          name: 'Staff',
          path: 'users/staff',
          meta: {
            title: 'Equipo',
          },
          component: () => import('@/views/dashboard/users/Staff'),
        },
        {
          name: 'Subscribers',
          path: 'users/subscribers',
          meta: {
            title: 'Suscriptores',
          },
          component: () => import('@/views/dashboard/users/Subscribers'),
        },
        // Locales
        {
          name: 'SalePoints',
          path: 'sale-points',
          meta: {
            title: 'Locales',
          },
          component: () => import('@/views/dashboard/salePoints/Index'),
        },
        {
          name: 'CreateSalePoint',
          path: 'sale-points/create',
          meta: {
            title: 'Nuevo Local',
          },
          component: () => import('@/views/dashboard/salePoints/CreateEdit'),
        },
        {
          name: 'EditSalePoint',
          path: 'sale-points/edit/:salePointId',
          meta: {
            title: 'Editar Local',
          },
          component: () => import('@/views/dashboard/salePoints/CreateEdit'),
        },
        // Info
        {
          name: 'Info',
          path: 'info',
          meta: {
            title: 'Info',
          },
          component: () => import('@/views/dashboard/info/Index'),
        },
        {
          name: 'InfoSliders',
          path: 'info/sliders',
          meta: {
            title: 'Sliders',
          },
          component: () => import('@/views/dashboard/info/Sliders'),
        },
        {
          name: 'InfoSocialMedia',
          path: 'info/social-media',
          meta: {
            title: 'Prensa',
          },
          component: () => import('@/views/dashboard/info/SocialMedia'),
        },
        {
          name: 'InfoFaqs',
          path: 'info/faqs',
          meta: {
            title: 'faq',
          },
          component: () => import('@/views/dashboard/info/Faqs'),
        },
        {
          name: 'InfoOurHistory',
          path: 'info/our-history',
          meta: {
            title: 'Nuestra Historia',
          },
          component: () => import('@/views/dashboard/info/OurHistory'),
        },
        {
          name: 'InfoTeam',
          path: 'info/team',
          meta: {
            title: 'Equipo',
          },
          component: () => import('@/views/dashboard/info/team/Index'),
        },
        // Audit
        {
          name: 'Audit',
          path: 'audit',
          meta: {
            title: 'Auditoría',
          },
          component: () => import('@/views/dashboard/Audit'),
        },
        // Notifications
        {
          name: 'Notifications',
          path: 'news',
          meta: {
            title: 'Notificaciones',
          },
          component: () => import('@/views/dashboard/Notifications'),
        },
      ],
    },
    {
      path: '/auth',
      component: () => import('@/views/pages/Index'),
      children: [{
          name: 'SignIn',
          path: 'sign-in',
          component: () => import('@/views/pages/auth/SignIn'),
        },
        {
          name: 'Login',
          path: 'login',
          component: () => import('@/views/pages/auth/Login'),
        },
        {
          name: 'PasswordChange',
          path: 'password-change/:userId',
          props: true,
          component: () => import('@/views/pages/auth/PasswordChange'),
        },
        {
          name: 'PasswordReset',
          path: 'password-reset',
          component: () => import('@/views/pages/auth/PasswordReset'),
        },
        {
          name: 'SendPasswordResetLink',
          path: 'send-password-reset-link',
          component: () => import('@/views/pages/auth/SendPasswordResetLink'),
        },
        {
          name: 'SignInConfirm',
          path: 'sign-in-confirm',
          component: () => import('@/views/pages/auth/SignInConfirm'),
        },
        {
          name: 'VerifyUser',
          path: 'verify-user',
          component: () => import('@/views/pages/auth/SignInConfirmed'),
        },
      ],
    },
    {
      path: '*',
      component: () => import('@/views/pages/Index'),
      children: [{
        name: '404 Error',
        path: '',
        component: () => import('@/views/pages/Error'),
      }],
    },
  ],
})

router.beforeEach((to, from, next) => {
  var session = localStorage.getItem(process.env.VUE_APP_SESSION_KEY)

  if ((!session || !JSON.parse(session)['session-id']) && to.matched.some(r => r.meta.requiresAuth)) {
    return next({
      name: 'HomeIndex',
    })
  }

  if (session) {
    session = JSON.parse(session)

    if (session['session-info']) {
      const sessionBytes = cryptoJs.DES.decrypt(session['session-info'].toString(), process.env.VUE_APP_BASE_ENCRYPT_KEY)
      const sessionData = JSON.parse(sessionBytes.toString(cryptoJs.enc.Utf8))
      const adminAllowed = (sessionData.is_superuser || sessionData.is_staff)

      if (to.matched.some(r => r.meta.admin) && !adminAllowed) {
        return next({
          name: 'Account',
        })
      }
    }

    if (to.path.toLowerCase().includes('auth') && !to.meta.requiresAuth) {
      return next({
        name: 'HomeIndex',
      })
    }
  }

  store.dispatch(LOAD_CART_FROM_STORAGE)
  if (session && !to.matched.some(r => r.meta.admin)) store.dispatch(IN_PROGRESS_ORDER)

  if (to.name === 'OrderDetail') {
    to.meta.title = `Pedido ${to.params.orderId}`
  } else if (to.name === 'CreateOrder') {
    to.meta.title = `Nuevo pedido ${to.params.deliveryType === 1 ? 'En local' : 'a domicilio'}`
  } else if (to.name === 'CustomerDetail') {
    const user = store.getters[CUSTOMER]
    to.meta.title = `${user.first_name} ${user.last_name}`
  }

  return next()
})

export default router
