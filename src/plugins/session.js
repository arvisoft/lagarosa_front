import Vue from 'vue'
import VueEasySession from 'vue-easysession'

const sessionOptions = {
  persist: true,
  keySession: process.env.VUE_APP_SESSION_KEY,
  expireSessionCallback () {
    location.href = '/'
  },
}

Vue.use(VueEasySession.install, sessionOptions)
