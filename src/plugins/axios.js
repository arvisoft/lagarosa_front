import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from '../router'
import cryptoJs from "crypto-js";

axios.interceptors.request.use((config) => {
  try {
    var session = localStorage.getItem(process.env.VUE_APP_SESSION_KEY)
    if (session) {
      session = JSON.parse(session)
      if (session['session-info']) {
        const sessionBytes = cryptoJs.DES.decrypt(session['session-info'].toString(), process.env.VUE_APP_BASE_ENCRYPT_KEY)
        const sessionData = JSON.parse(sessionBytes.toString(cryptoJs.enc.Utf8))

        config.headers['Authorization'] = `Token ${sessionData.auth_token}`
      }
    }
  } catch (error) {
    console.error(error)
  }

  return config;
}, (error) => {
  return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
  return response
}, (error) => {
  if (error.message.includes('401')) {
    localStorage.clear()
    return router.push({
      name: 'HomeIndex',
    })
  }
  return Promise.reject(error)
})

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
Vue.use(VueAxios, axios)
