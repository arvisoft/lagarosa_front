import Vue from 'vue'
import * as rules from 'vee-validate/dist/rules'
import {
  localize,
  extend,
  ValidationObserver,
  ValidationProvider,
} from 'vee-validate';


function loadLocale(code) {
  return import(`vee-validate/dist/locale/${code}.json`).then(locale => {
    localize(code, locale)
  });
}

// loadLocale(localStorage.getItem('usr-lang') || "es")
loadLocale('es')

Object.keys(rules).forEach(rule => {
  extend(rule, {
    ...rules[rule],
  })
});

Vue.component('validation-provider', ValidationProvider)
Vue.component('validation-observer', ValidationObserver)
