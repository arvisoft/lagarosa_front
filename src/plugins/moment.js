import Vue from 'vue'
import VueMoment from 'vue-moment'
import moment from 'moment'

Vue.use(VueMoment, {
  moment,
})

// Vue.moment.locale(localStorage.getItem('usr-lang'))
Vue.moment.locale('es-ES')
