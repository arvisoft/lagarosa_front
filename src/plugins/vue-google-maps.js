import Vue from 'vue'
import VueGoogleMap from 'vuejs-google-maps'

Vue.use(VueGoogleMap, {
  load: {
    libraries: ['drawing', 'places'],
    apiKey: process.env.VUE_APP_GOOGLE_API_KEY,
  },
})
