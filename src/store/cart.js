import Vue from 'vue'
import axios from 'axios'
import {
  UserUrls,
  StoreUrls,
  GralInfoUrls,
} from '../UrlConstants'
import {
  omit,
  pick,
} from 'lodash'
import {
  DELIVERY,
  NUM_ITEMS,
  CART_TOTAL,
  CLEAR_CART,
  CART_ITEMS,
  PAYMENT_TYPE,
  DELETE_PRODUCT,
  DELIVERY_BASE,
  DELIVERY_SETUP,
  DELIVERY_ORIGIN,
  DELIVERY_ADDRESS,
  DELIVERY_DATE,
  ADD_ITEM_TO_CART,
  GIFT_INFORMATION,
  PAYMENT_TYPE_CASH,
  IN_PROGRESS_ORDER,
  SUGGESTED_PRODUCTS,
  PREFERRED_SALE_POINT,
  CALCULATING_DELIVERY,
  REMOVE_ITEM_FROM_CART,
  LOAD_CART_FROM_STORAGE,
  ALLOWED_TO_BUY_BY_TIME,
} from "./cart-constants";

const saveCart = (items) => {
  window.localStorage.setItem('cart', JSON.stringify(items))
}

const saveSelectedStore = (store) => {
  if (store) window.localStorage.setItem('store', JSON.stringify(store))
  else window.localStorage.removeItem('store')
}

const savePreferredSalePoint = (id) => {
  window.sessionStorage.setItem('preferred-sale-point', id)
}

export default {
  state: {
    items: [],
    origin: {},
    address: {},
    delivery: 0,
    deliveryBase: 0,
    deliveryIncrease: 0,
    deliveryDate: '',
    preferredSalePoint: 0,
    paymentType: PAYMENT_TYPE_CASH,
    giftInfo: {
      isGift: false,
      message: '',
      receiverName: '',
    },
    currentOrder: {},
    allowedToBuyByTime: false,
    calculatingDelivery: false,
    suggestedProducts: []
  },
  getters: {
    [CART_ITEMS]: state => state.items,
    [DELIVERY]: stete => stete.delivery,
    [DELIVERY_ORIGIN]: state => state.origin,
    [DELIVERY_ADDRESS]: state => state.address,
    [PAYMENT_TYPE]: state => state.paymentType,
    [GIFT_INFORMATION]: state => state.giftInfo,
    [DELIVERY_DATE]: state => state.deliveryDate,
    [DELIVERY_BASE]: stete => stete.deliveryBase,
    [IN_PROGRESS_ORDER]: state => state.currentOrder || {},
    [SUGGESTED_PRODUCTS]: state => state.suggestedProducts,
    [CALCULATING_DELIVERY]: state => state.calculatingDelivery,
    [ALLOWED_TO_BUY_BY_TIME]: state => state.allowedToBuyByTime,
    [PREFERRED_SALE_POINT]: state => parseInt(state.preferredSalePoint),
    [NUM_ITEMS]: state => state.items.reduce((total, product) => (total += product.quantity), 0),
    [CART_TOTAL]: state => state.items.reduce((total, product) => (total += product.quantity * product.price), 0),
  },
  mutations: {
    [ADD_ITEM_TO_CART](state, {
      item,
      detail,
      quantity,
    }) {
      if (!state.items.length) {
        state.origin = omit(item.sale_point, ['is_active', 'created_at', 'updated_at', 'city'])
        saveSelectedStore(state.origin)
      }

      const product = item.product
      const productIdx = state.items.findIndex(p => p.id === product.id)

      if (productIdx >= 0) {
        Vue.set(state.items[productIdx], 'quantity', state.items[productIdx].quantity + quantity)
      } else {
        state.items.push({
          detail,
          quantity,
          ...pick(product, ['id', 'images', 'name', 'description', 'product_type', 'price', 'restriction_message']),
        })
      }
      fbq('track', 'AddToCart');
      gtag('event', 'add_to_cart', {
        items: [product],
        value: product.price
      });
      saveCart(state.items)
    },
    [REMOVE_ITEM_FROM_CART](state, itemId) {
      var productIdx = state.items.findIndex(p => p.id === itemId)

      if (productIdx >= 0) {
        const finalQuantity = state.items[productIdx].quantity - 1

        if (finalQuantity >= 1) {
          Vue.set(state.items[productIdx], 'quantity', finalQuantity)
        } else {
          Vue.delete(state.items, productIdx)
        }

        if (!state.items.length) {
          state.address = {};
          state.origin = null;
          state.delivery = 0;
          saveSelectedStore(null)
        }
        saveCart(state.items)
      }
    },
    [DELETE_PRODUCT](state, itemId) {
      var productIdx = state.items.findIndex(p => p.id === itemId)

      if (productIdx >= 0) {
        Vue.delete(state.items, productIdx)

        if (!state.items.length) {
          state.address = {}
          state.origin = null
          state.delivery = 0
          saveSelectedStore(null)
        }

        saveCart(state.items)
      }
    },
    [LOAD_CART_FROM_STORAGE](state) {
      const items = JSON.parse(window.localStorage.getItem('cart'))
      const store = JSON.parse(window.localStorage.getItem('store'))

      state.origin = store
      state.items = Array.isArray(items) && items || []
    },
    [CLEAR_CART](state) {
      state.items = []
      state.origin = {}
      state.address = {}
      state.delivery = 0
      state.deliveryDate = ''
      state.giftInfo = {
        isGift: false,
        message: '',
        receiverName: '',
      }
      state.paymentType = PAYMENT_TYPE_CASH
      saveCart(state.items)
      saveSelectedStore(null)
    },
    [DELIVERY_ADDRESS](state, payload) {
      state.address = payload
    },
    [DELIVERY_ORIGIN](state, payload) {
      state.origin = payload
    },
    [DELIVERY](state, payload) {
      state.delivery = payload
    },
    [GIFT_INFORMATION](state, payload) {
      state.giftInfo = payload
    },
    [PAYMENT_TYPE](state, payload) {
      state.paymentType = payload
    },
    [DELIVERY_SETUP](state, {
      base,
      increase,
    }) {
      state.deliveryBase = base || 2500
      state.deliveryIncrease = increase || 500
    },
    [IN_PROGRESS_ORDER](state, payload) {
      state.currentOrder = payload
    },
    [PREFERRED_SALE_POINT](state, payload) {
      state.preferredSalePoint = payload
      savePreferredSalePoint(payload)
    },
    [DELIVERY_DATE](state, payload) {
      state.deliveryDate = payload
    },
    [ALLOWED_TO_BUY_BY_TIME](state, payload) {
      state.allowedToBuyByTime = payload
    },
    [CALCULATING_DELIVERY](state, payload) {
      state.calculatingDelivery = payload
    },
    [SUGGESTED_PRODUCTS](state, payload) {
      state.suggestedProducts = payload
    },
  },
  actions: {
    [ADD_ITEM_TO_CART]({
      commit,
    }, {
      item,
      detail = [],
      quantity = 1,
    }) {
      commit(ADD_ITEM_TO_CART, {
        item,
        detail,
        quantity,
      })
    },
    [REMOVE_ITEM_FROM_CART]({
      commit,
    }, productId) {
      commit(REMOVE_ITEM_FROM_CART, productId)
    },
    [DELETE_PRODUCT]({
      commit,
    }, productId) {
      commit(DELETE_PRODUCT, productId)
    },
    [LOAD_CART_FROM_STORAGE]({
      commit,
    }) {
      commit(LOAD_CART_FROM_STORAGE)
    },
    [CLEAR_CART]({
      commit,
    }) {
      commit(CLEAR_CART)
    },
    [DELIVERY]({
      state,
      commit,
    }, destination) {
      const base = state.deliveryBase || 2500
      const increase = state.deliveryIncrease || 500

      if (state.origin && state.origin.address) {
        commit(CALCULATING_DELIVERY, true)

        axios.post(UserUrls.distanceForDelivery, {
            destination,
            source: state.origin.address,
          })
          .then(response => {
            if (response.data.status === 'OK') {
              try {
                var distance = response.data.rows[0].elements[0].distance.value / 1000 // distance in Km
                var delivery = distance <= 1 && base || base + increase * (Math.trunc(distance) - 1)

                commit(DELIVERY, delivery)
              } catch (error) {
                commit(DELIVERY, base)
              }
            } else {
              commit(DELIVERY, base)
            }
          })
          .finally(() => commit(CALCULATING_DELIVERY, false))
      } else {
        commit(DELIVERY, base)
      }
    },
    [PREFERRED_SALE_POINT]({
      commit,
    }) {
      const sp = window.sessionStorage.getItem('preferred-sale-point')
      commit(PREFERRED_SALE_POINT, sp || 0)
    },
    [DELIVERY_ORIGIN]({
      commit,
    }, salePoint) {
      commit(DELIVERY_ORIGIN, salePoint)
    },
    [DELIVERY_ADDRESS]({
      commit,
    }, address) {
      commit(DELIVERY_ADDRESS, address)
    },
    [GIFT_INFORMATION]({
      commit,
    }, giftInformation) {
      commit(GIFT_INFORMATION, giftInformation)
    },
    [DELIVERY_SETUP]({
      commit,
    }) {
      axios.get(`${GralInfoUrls.config}?key=delivery`)
        .then(response => {
          const deliveryInfo = response.data[0]

          if (deliveryInfo) {
            commit(DELIVERY_SETUP, {
              base: deliveryInfo.value.first_km,
              increase: deliveryInfo.value.additional_km,
            })
            commit(DELIVERY, deliveryInfo.value.first_km)
          }
        })
    },
    [IN_PROGRESS_ORDER]({
      commit,
    }) {
      axios.get(`${StoreUrls.order}?order_active=True`)
        .then(response => commit(IN_PROGRESS_ORDER, response.data[0]))
    },
  },
}
