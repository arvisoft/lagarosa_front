export const NUM_ITEMS = 'numItems'
export const CLEAR_CART = 'clearCart'
export const CART_ITEMS = 'cartItems'
export const CART_TOTAL = 'cartTotal'
export const DELIVERY = 'deliveryAmmount'
export const PAYMENT_TYPE = 'paymentType'
export const GIFT_INFORMATION = 'giftInfo'
export const DELETE_PRODUCT = 'deleteProduct'
export const ADD_ITEM_TO_CART = 'addItemToCart'
export const DELIVERY_ORIGIN = 'deliveryOrigin'
export const DELIVERY_DATE = 'deliveryDate'
export const DELIVERY_ADDRESS = 'deliveryAddress'
export const DELIVERY_SETUP = 'deliveryBase'
export const DELIVERY_BASE = 'deliveryBasePrice'
export const REMOVE_ITEM_FROM_CART = 'removeItemFromCart'
export const LOAD_CART_FROM_STORAGE = 'loadCartFromStorage'
export const CALCULATING_DELIVERY = 'calculateDelivery'
export const SUGGESTED_PRODUCTS = 'suggestedProducts'

export const PAYMENT_TYPE_CASH = 'cash'
export const PAYMENT_TYPE_ONLINE = 'online'
export const PAYMENT_METHOD_UPON_DELIVERY = 'uponDelivery'
export const PAYMENT_METHOD_UPON_DELIVERY_ID = 1
export const PAYMENT_METHOD_DATAPHONE = 'dataphone'
export const PAYMENT_METHOD_DATAPHONE_ID = 2
export const PAYMENT_METHOD_CREDIT_CARD = 'creditCard'
export const PAYMENT_METHOD_CREDIT_CARD_ID = 3
export const PAYMENT_METHOD_PSE = 'pse'
export const PAYMENT_METHOD_PSE_ID = 4
export const PAYMENT_METHOD_NEQUI = 'nequi'
export const PAYMENT_METHOD_NEQUI_ID = 5
export const PAYMENT_METHOD_BANCOLOMBIA_TRANSFER = 'bancolombiaTransfer'
export const PAYMENT_METHOD_BANCOLOMBIA_TRANSFER_ID = 6
export const PAYMENT_METHOD_BANCOLOMBIA_QR = 'bancolombiaQr'
export const PAYMENT_METHOD_BANCOLOMBIA_QR_ID = 7

export const IN_PROGRESS_ORDER = 'checkInProgressOrder'
export const PREFERRED_SALE_POINT = 'preferredSalePoint'
export const ALLOWED_TO_BUY_BY_TIME = 'allowedToBuyByTime'


export const GEST_USER = 'gestUserInfo'
export const CRUD_ADDRESS = 'crudAddress'
export const USER_ADDRESSES = 'userAddresses'
