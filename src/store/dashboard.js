import Vue from 'vue'
import {
  forOwn,
  isArray,
  groupBy,
} from 'lodash'
import {
  getPaymentMethod,
} from '@/utils'

export const TEAM_MEMBER = 'teamMember'
export const PRODUCT = 'product'
export const PRODUCT_HAS_OPTIONS = 'hasProductOptions'
export const CUSTOM_PRODUCT_STEPS = 'customProductSteps'
export const SET_INGREDIENT_PRICE = 'changeIngredientPrice'
export const CUSTOM_PRODUCT_ADD_STEP = 'customProductaddStep'
export const CUSTOM_PRODUCT_ADD_INGREDIENT = 'customProductaddIngredient'
export const SALE_POINT = 'salePoint'
export const SALE_POINT_TOTAL_SEATING = 'totalSeating'
export const ORDER = 'order'
export const CUSTOMER = 'customer'
export const UNREAD_NOTIFICATION = 'unreadNotification'

export default {
  state: {
    unreadNotification: false,
    product: {
      id: null,
      images: [],
      parent_products: [],
      ingredients: [],
      category: [],
      name: null,
      description: null,
      product_type: null,
      unit_limit: 1,
      price: 1,
      flavor: {},
      has_options: false,
    },
    teamMember: {},
    customProductSteps: {
      1: {
        id: null,
        choice: 1,
        ingredients: [{
          price: 0,
          name: null,
        }],
      },
    },
    salePoint: {
      id: null,
      name: '',
      city: {},
      latitude: 0,
      seatings: 0,
      longitude: 0,
      schedules: [],
      thumbnail: '',
      description: '',
      is_principal: false,
    },
    order: {
      id: 0,
      user: {},
      status: {},
      address: {},
      order_details: [],
      total_price: 0,
      order_type: 0,
      delivery_price: 0,
      delivery_name: '',
      message: null,
      rating: 0,
      comment: '',
      annotations: '',
      payment_method: 0,
      payment_method_name: '',
    },
    customer: {
      id: 0,
      phone: '',
      email: '',
      last_name: '',
      first_name: '',
      date_joined: '',
    },
  },
  getters: {
    [ORDER]: state => state.order,
    [PRODUCT]: state => state.product,
    [CUSTOMER]: state => state.customer,
    [SALE_POINT]: state => state.salePoint,
    [TEAM_MEMBER]: state => state.teamMember,
    [UNREAD_NOTIFICATION]: state => state.unreadNotification,
    [CUSTOM_PRODUCT_STEPS]: state => state.customProductSteps,
    [PRODUCT_HAS_OPTIONS]: state => !!state.product.parent_products.length > 0,
  },
  mutations: {
    [UNREAD_NOTIFICATION](state, payload) {
      state.unreadNotification = payload
    },
    [TEAM_MEMBER](state, payload) {
      state.teamMember = payload
    },
    [CUSTOMER](state, payload) {
      state.customer = payload
    },
    [PRODUCT](state, payload) {
      state.product = payload

      Vue.set(state.product, 'category', payload.category.map(cat => ({
        id: cat.id,
        name: cat.category,
      })))

      Vue.set(state.product, 'flavor', payload.flavor ? {
        id: payload.flavor.id,
        name: payload.flavor.flavor,
      } : {})

      if (!state.product.ingredients) state.product.ingredients = []

      var steps = groupBy(state.product.ingredients, 'step')

      // si es nuevo
      if (!steps[1]) {
        steps[1] = {
          id: null,
          choice: 1,
          ingredients: [{
            price: 0,
            name: null,
          }],
        }
      } else {
        const info = {}
        forOwn(steps, (value, key) => {
          info[key] = {
            ingredients: value,
            choice: value[0].choice,
          }
        })

        steps = info
      }
      state.customProductSteps = steps
    },
    [CUSTOM_PRODUCT_ADD_STEP](state) {
      const totalSteps = Object.keys(state.customProductSteps).length
      Vue.set(state.customProductSteps, `${totalSteps + 1}`, {
        id: null,
        choice: 1,
        ingredients: [{
          price: 0,
          name: null,
        }],
      })
    },
    [CUSTOM_PRODUCT_ADD_INGREDIENT](state, payload) {
      var i = state.customProductSteps[payload].ingredients.length

      Vue.set(state.customProductSteps[payload].ingredients, i, {
        price: 0,
        name: null,
      })
    },
    [SET_INGREDIENT_PRICE](state, {
      step,
      index,
      price,
    }) {
      const ingredient = state.customProductSteps[step].ingredients[index]
      const finalPrice = (ingredient.price <= 0 && price < 1) ? 0 : ingredient.price + price

      Vue.set(state.customProductSteps[step].ingredients[index], 'price', finalPrice)
    },
    [SALE_POINT](state, payload) {
      state.salePoint = payload
      Vue.set(state.salePoint, 'seatings', payload.seatings && isArray(payload.seatings) ? payload.seatings.length : 0)
    },
    [ORDER](state, payload) {
      state.order = payload

      Vue.set(state.order, 'payment_method_name', getPaymentMethod(payload))
    },
  },
}
