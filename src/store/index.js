import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import cart from './cart'
import dashboard from './dashboard'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    cart,
    user,
    dashboard,
  },
  state: {
    mainMenuLinks: require('@/data/mainMenuLinks.json'),
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: 'https://demos.creative-tim.com/material-dashboard-pro/assets/img/sidebar-1.jpg',
    adminDrawer: null,
    mainDrawer: false,
  },
  mutations: {
    SET_BAR_IMAGE: (state, payload) => (state.barImage = payload),
    SET_ADMIN_DRAWER: (state, payload) => (state.adminDrawer = payload),
    SET_SCRIM: (state, payload) => (state.barColor = payload),
    SET_MAIN_DRAWER: (state, payload) => (state.mainDrawer = payload),
    TOGGLE_MAIN_DRAWER: (state) => (state.mainDrawer = !state.mainDrawer),
  },
  getters: {
    mainMenuLinks: state => {
      const links = []

      for (const link of state.mainMenuLinks) {
        if (links.find(li => li.path === link.path)) continue

        links.push(link)
      }

      return links
    },
  },
  actions: {

  },
})
