import axios from 'axios'
import {
  UserUrls,
} from '../UrlConstants'

import {
  GEST_USER,
  CRUD_ADDRESS,
  USER_ADDRESSES
} from "./cart-constants";

export default {
  state: () => ({
    crudAddress: {},
    addresses: [],
    guestInfo: {
      guest_address: {
        city: "",
        state: "",
        is_principal: true,
        notes: "",
        address: "",
        latitude: 1,
        longitude: 1
      },
      guest_name: "",
      guest_phone: "",
      guest_email: ""
    }
  }),
  mutations: {
    [USER_ADDRESSES]: (state, payload) => (state.addresses = payload),
    [CRUD_ADDRESS]: (state, payload) => (state.crudAddress = payload),
    [GEST_USER]: (state, payload) => (state.guestInfo = payload),
  },
  getters: {
    [USER_ADDRESSES]: state => state.addresses,
    [CRUD_ADDRESS]: state => state.crudAddress,
    [GEST_USER]: state => state.guestInfo,
  },
  actions: {
    [USER_ADDRESSES]({
      commit,
    }) {
      return axios.get(UserUrls.address)
        .then(response => {
          commit(USER_ADDRESSES, response.data)
        })
        .catch(error => {
          console.warn(error)
        })
    },
  },
}
