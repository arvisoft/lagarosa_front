// =========================================================
// * Vuetify Material Dashboard PRO - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard-pro
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import i18n from './i18n'
import './plugins/base'
import './plugins/vee-validate'
import './plugins/moment'
import './plugins/axios'
import './plugins/session'
import './plugins/kinesis'
import './plugins/vue-google-maps'
import './plugins/vue-recaptcha-v3'
import './plugins/vue-google-charts'
import './plugins/printer'
import './mixins/globals'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app')
